#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#install vim
sudo apt install -y vim

#install visual studio code according to https://code.visualstudio.com/docs/setup/linux
sudo apt install -y curl
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt update
sudo apt install -y code
code --install-extension dan-c-underwood.arm --force
code --install-extension ms-vscode.cpptools --force
code --install-extension editorconfig.editorconfig --force

#install terminal emulations
sudo apt install -y minicom
sudo chmod +s /usr/bin/minicom

#install some ubuntu tools
sudo apt install -y g++ autoconf libtool device-tree-compiler make patch libglib2.0-dev u-boot-tools 
sudo apt install -y libncurses5-dev mercurial flex bison swig python-dev python3-dev libyaml-dev libssl-dev

## make additional symbolic links for former libraries used by aarch64 gdb 
## probably to be removed with newest buildroot version
sudo ln -s /usr/lib/x86_64-linux-gnu/libtinfo.so.6.2 /usr/lib/x86_64-linux-gnu/libtinfo.so.5 
sudo ln -s /usr/lib/x86_64-linux-gnu/libncursesw.so.6 /usr/lib/x86_64-linux-gnu/libncursesw.so.5

#install git tools and configure keyring
sudo apt install -y git git-gui meld
sudo apt install -y libsecret-1-0 libsecret-1-dev
sudo make -C /usr/share/doc/git/contrib/credential/libsecret
git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret

# #install java environment
# sudo apt install -y default-jre

# #install eclipse IDE
# if [ ! -d "/opt/eclipse" ]; then
# wget http://mirrors.uniri.hr/eclipse//technology/epp/downloads/release/photon/R/eclipse-cpp-photon-R-linux-gtk-x86_64.tar.gz
# sudo tar xf eclipse-cpp-photon-R-linux-gtk-x86_64.tar.gz -C /opt
# sudo ln -s /opt/eclipse/eclipse /usr/bin/eclipse
# rm eclipse-cpp-photon-R-linux-gtk-x86_64.tar.gz
# fi
# cp ${SCRIPTDIR}/config/eclipse.desktop ~/.local/share/applications/

#install tftpd server
sudo apt install -y xinetd tftpd tftp 
sudo cp ${SCRIPTDIR}/config/tftp /etc/xinetd.d/
sudo service xinetd start 

#install nfs server
sudo apt install -y nfs-kernel-server
sudo ${SCRIPTDIR}/config/update_exports.py
sudo systemctl restart nfs-kernel-server
#service nfs-server status

