#!/usr/bin/python

import os

def config_nfs():
	src = '/etc/exports'
	tmp = '/etc/exports.temp'
	home = "/home/lmi/workspace"

	f = open(src, 'r')
	o = open(tmp, 'w')

	for l in f:
		if home in l:
			continue
		o.write (l)

	o.write (home + " 192.168.0.0/16(rw,no_root_squash,sync,no_subtree_check)\n")

	f.close()
	o.close()
	os.rename (tmp, src)


if __name__ == "__main__":
	config_nfs();

