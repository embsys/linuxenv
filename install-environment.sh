#!/bin/bash
os=$(grep "NAME" /etc/os-release)
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ $os = *"Ubuntu"* ]]; then
echo "install Ubuntu development environment "
${SCRIPTDIR}/install-ubuntu-environment.sh

elif [[ $os = *"Fedora"* ]]; then
echo "install Fedora development environment "
${SCRIPTDIR}/install-fedora-environment.sh 

else
echo "no matching os found!"
exit 1
fi