#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "${SCRIPTDIR}"

#install editors
sudo dnf install -y vim

#install visual studio code according to https://code.visualstudio.com/docs/setup/linux
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf check-update
sudo dnf install -y code
code --install-extension dan-c-underwood.arm --force
code --install-extension ms-vscode.cpptools --force
code --install-extension editorconfig.editorconfig --force

#install terminal emulations
sudo dnf install -y minicom
sudo chmod +s /usr/bin/minicom

#install some fedora tools
sudo dnf install -y gnome-tweak-tool firewall-config kernel-devel libyaml-devel perl
sudo dnf install -y gcc-c++ autoconf libtool dtc make patch ctags openssl-devel uboot-tools ncurses-devel mercurial
sudo dnf install -y flex bison swig python-devel python3-devel policycoreutils-python-utils perl-ExtUtils-MakeMaker

#install git tools and configure keyring
sudo dnf install -y git-gui meld
sudo dnf install -y git-credential-libsecret
git config --global credential.helper /usr/libexec/git-core/git-credential-libsecret

#install tftpd server
sudo dnf install -y tftp-server tftp 
sudo cp ${SCRIPTDIR}/config/tftp-server.* /etc/systemd/system
sudo systemctl daemon-load
sudo systemctl enable --now tftp-server
sudo setsebool -P tftp_anon_write 1
sudo setsebool -P tftp_home_dir 1

#install nfs server
sudo dnf install -y nfs-utils libnfs-utils
sudo ${SCRIPTDIR}/config/update_exports.py
sudo systemctl start rpcbind nfs-server
sudo systemctl enable rpcbind nfs-server

#configure firewall
sudo firewall-cmd --permanent --zone FedoraWorkstation --add-service nfs
sudo firewall-cmd --permanent --zone FedoraWorkstation --add-service tftp
sudo firewall-cmd --permanent --zone FedoraWorkstation --add-service rpc-bind
sudo firewall-cmd --reload
